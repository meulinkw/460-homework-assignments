﻿[assembly: OwinStartupAttribute(typeof(MVC_Applicationhw4.Startup))]
namespace MVC_Applicationhw4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
