﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Applicationhw4.Controllers
{


    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult About()
        {
            return View();
        }
       
        public ActionResult GetPage()
        {

            return View();
        }
        public ActionResult Contact()
        {

            return View();
        }


        public ActionResult LoanCalc()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LoanCalc(int? principal, double? interest, int? term)
        {
            // Assign the input values to temp variables or give the value of -1 if null
            int p = principal ?? -1;
            double i = interest ?? -1.0;
            int t = term ?? -1;
            // If any of the values were null, send an error message to the view
            if ((p == -1) | (i == -1.0) | (t == -1))
            {
                ViewBag.Error = "Please enter a value in all fields";
                return View();
            }

            // Calculate the monthly payment
            double monthly = CalculateMonthly(p, i, t);

            // Add the calculated values to the ViewBag
            ViewBag.Sum = Math.Round(monthly * (t * 12), 2);
            ViewBag.Monthly = Math.Round(monthly, 2);

            return View();
        }

        private double CalculateMonthly(int p, double i, int t)
        {
            double monthlyInterest = (i / 100) / 12;
            double payments = t * 12;

            return p * (monthlyInterest / (1 - Math.Pow((1 + monthlyInterest), -payments)));
        }
    }
}