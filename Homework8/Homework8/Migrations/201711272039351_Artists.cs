namespace Homework8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Artists : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Classification", "Artwork", "dbo.Artwork");
            DropForeignKey("dbo.Artwork", "Artist", "dbo.Artist");
            DropForeignKey("dbo.Classification", "Genre", "dbo.Genre");
            DropIndex("dbo.ArtWork", new[] { "Artist" });
            DropIndex("dbo.Classification", new[] { "Artwork" });
            DropIndex("dbo.Classification", new[] { "Genre" });
            DropPrimaryKey("dbo.Artist");
            DropPrimaryKey("dbo.Genre");
            DropPrimaryKey("dbo.ArtWork");
            AddColumn("dbo.Artist", "ArtistID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Genre", "GenreID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.ArtWork", "ArtWorkID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Artist", "Name", c => c.String(nullable: false, maxLength: 55, unicode: false));
            AlterColumn("dbo.Artist", "BirthCity", c => c.String(maxLength: 55, unicode: false));
            AlterColumn("dbo.Classification", "ArtWork", c => c.String(maxLength: 55, unicode: false));
            AlterColumn("dbo.Classification", "Genre", c => c.String(maxLength: 55, unicode: false));
            AlterColumn("dbo.Genre", "Name", c => c.String(nullable: false, maxLength: 55, unicode: false));
            AlterColumn("dbo.ArtWork", "Title", c => c.String(nullable: false, maxLength: 55, unicode: false));
            AlterColumn("dbo.ArtWork", "Artist", c => c.String(maxLength: 55, unicode: false));
            AddPrimaryKey("dbo.Artist", "Name");
            AddPrimaryKey("dbo.Genre", "Name");
            AddPrimaryKey("dbo.ArtWork", "Title");
            CreateIndex("dbo.ArtWork", "Artist");
            CreateIndex("dbo.Classification", "Genre");
            AddForeignKey("dbo.ArtWork", "Artist", "dbo.Artist", "Name");
            AddForeignKey("dbo.Classification", "Genre", "dbo.Genre", "Name");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classification", "Genre", "dbo.Genre");
            DropForeignKey("dbo.ArtWork", "Artist", "dbo.Artist");
            DropIndex("dbo.Classification", new[] { "Genre" });
            DropIndex("dbo.ArtWork", new[] { "Artist" });
            DropPrimaryKey("dbo.ArtWork");
            DropPrimaryKey("dbo.Genre");
            DropPrimaryKey("dbo.Artist");
            AlterColumn("dbo.ArtWork", "Artist", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.ArtWork", "Title", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.Genre", "Name", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.Classification", "Genre", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Classification", "ArtWork", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Artist", "BirthCity", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Artist", "Name", c => c.String(nullable: false, maxLength: 255, unicode: false));
            DropColumn("dbo.ArtWork", "ArtWorkID");
            DropColumn("dbo.Genre", "GenreID");
            DropColumn("dbo.Artist", "ArtistID");
            AddPrimaryKey("dbo.ArtWork", "Title");
            AddPrimaryKey("dbo.Genre", "Name");
            AddPrimaryKey("dbo.Artist", "Name");
            CreateIndex("dbo.Classification", "Genre");
            CreateIndex("dbo.Classification", "Artwork");
            CreateIndex("dbo.ArtWork", "Artist");
            AddForeignKey("dbo.Classification", "Genre", "dbo.Genre", "Name");
            AddForeignKey("dbo.Artwork", "Artist", "dbo.Artist", "Name");
            AddForeignKey("dbo.Classification", "Artwork", "dbo.Artwork", "Title");
        }
    }
}
