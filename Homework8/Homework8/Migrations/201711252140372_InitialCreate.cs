namespace Homework8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Artist",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 255, unicode: false),
                        BirthDate = c.DateTime(storeType: "date"),
                        BirthCity = c.String(maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.Artwork",
                c => new
                    {
                        Title = c.String(nullable: false, maxLength: 255, unicode: false),
                        Artist = c.String(maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.Title)
                .ForeignKey("dbo.Artist", t => t.Artist)
                .Index(t => t.Artist);
            
            CreateTable(
                "dbo.Classification",
                c => new
                    {
                        ClassificationID = c.Int(nullable: false, identity: true),
                        Artwork = c.String(maxLength: 255, unicode: false),
                        Genre = c.String(maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.ClassificationID)
                .ForeignKey("dbo.Genre", t => t.Genre)
                .ForeignKey("dbo.Artwork", t => t.Artwork)
                .Index(t => t.Artwork)
                .Index(t => t.Genre);
            
            CreateTable(
                "dbo.Genre",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.Name);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Artwork", "Artist", "dbo.Artist");
            DropForeignKey("dbo.Classification", "Artwork", "dbo.Artwork");
            DropForeignKey("dbo.Classification", "Genre", "dbo.Genre");
            DropIndex("dbo.Classification", new[] { "Genre" });
            DropIndex("dbo.Classification", new[] { "Artwork" });
            DropIndex("dbo.Artwork", new[] { "Artist" });
            DropTable("dbo.Genre");
            DropTable("dbo.Classification");
            DropTable("dbo.Artwork");
            DropTable("dbo.Artist");
        }
    }
}
