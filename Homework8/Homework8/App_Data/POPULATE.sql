﻿INSERT INTO Artist (Name, BirthDate, BirthCity) VALUES ('M.C. Escher','06/17/1898','Leeuwarden, Netherlands');
INSERT INTO Artist (Name, BirthDate, BirthCity) VALUES ('Leonardo Da Vinci', '05/02/1519','Vinci, Italy');
INSERT INTO Artist (Name, BirthDate, BirthCity) VALUES ('Hatip Mehmed Efendi', '11/18/1680', 'unknown');
INSERT INTO Artist (Name, BirthDate, BirthCity) VALUES ('Salvador Dali','05/11/1904','Figueres, Spain');

SELECT * FROM Artist;

INSERT INTO Artwork (Title, Artist) VALUES ('Circle Limit III', 'M.C. Escher');
INSERT INTO Artwork (Title, Artist) VALUES ('Twon Tree', 'M.C. Escher');
INSERT INTO Artwork (Title, Artist) VALUES ('Mona Lisa', 'Leonardo Da Vinci');
INSERT INTO Artwork (Title, Artist) VALUES ('The Virtruvian Man', 'Leonardo Da Vinci');
INSERT INTO Artwork (Title, Artist) VALUES ('Ebru', 'Hatip Mehmed Efendi');
INSERT INTO Artwork (Title, Artist) VALUES ('Honey Is Sweeter Than Blood', 'Salvador Dali');

SELECT * FROM Artwork;

INSERT INTO Genre (Name) VALUES ('Tesselation');
INSERT INTO Genre (Name) VALUES ('Surrealism');
INSERT INTO Genre (Name) VALUES ('Portrait');
INSERT INTO Genre (Name) VALUES ('Renaissance');

SELECT * FROM Genre;

INSERT INTO Classification (Artwork, Genre) VALUES ('Circle Limit III', 'Tesselation');
INSERT INTO Classification (Artwork, Genre) VALUES ('Twon Tree', 'Tesselation');
INSERT INTO Classification (Artwork, Genre) VALUES ('Twon Tree', 'Surrealism');
INSERT INTO Classification (Artwork, Genre) VALUES ('Mona Lisa', 'Renaissance');
INSERT INTO Classification (Artwork, Genre) VALUES ('The Virtruvian Man', 'Renaissance');
Insert INTO Classification (Artwork, Genre) VALUES ('Ebru', 'Tesselation');
INSERT INTO Classification (Artwork, Genre) VALUES ('Honey Is Sweeter Than Blood', 'Surrealism');

SELECT * FROM Classification;