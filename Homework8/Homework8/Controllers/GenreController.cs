﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework8.Controllers
{
    public class GenreController : Controller
    {
        private DBContext db = new DBContext();

        // GET: Genre
        public JsonResult Index(string genre)
        {

            var artWorks = db.Classifications.Where(c => c.Genre1.Name == genre).Select(c => c.ArtWork);
            var data = new List<KeyValuePair<string, string>>();

            foreach(var artwork in artWorks)
            {
                var artist = db.ArtWorks.First(a => a.Title == artwork).Artist;
                data.Add(new KeyValuePair<string, string>(artist, artwork));
            }
           
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}