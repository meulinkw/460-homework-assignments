namespace Homework8
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ArtWork")]
    public partial class ArtWork
    {
        [Key]
        [StringLength(55)]
        public string Title { get; set; }

        [StringLength(55)]
        public string Artist { get; set; }

        public virtual Artist Artist1 { get; set; }
    }
}
