namespace Homework8
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Classification")]
    public partial class Classification
    {
        public int ClassificationID { get; set; }

        [StringLength(55)]
        public string ArtWork { get; set; }

        [StringLength(55)]
        public string Genre { get; set; }

        public virtual Genre Genre1 { get; set; }
    }
}
