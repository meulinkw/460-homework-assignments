namespace Homework8
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<Artist> Artists { get; set; }
        public virtual DbSet<ArtWork> ArtWorks { get; set; }
        public virtual DbSet<Classification> Classifications { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artist>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Artist>()
                .Property(e => e.BirthCity)
                .IsUnicode(false);

            modelBuilder.Entity<Artist>()
                .HasMany(e => e.ArtWorks)
                .WithOptional(e => e.Artist1)
                .HasForeignKey(e => e.Artist);

            modelBuilder.Entity<ArtWork>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<ArtWork>()
                .Property(e => e.Artist)
                .IsUnicode(false);

            modelBuilder.Entity<Classification>()
                .Property(e => e.ArtWork)
                .IsUnicode(false);

            modelBuilder.Entity<Classification>()
                .Property(e => e.Genre)
                .IsUnicode(false);

            modelBuilder.Entity<Genre>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Genre>()
                .HasMany(e => e.Classifications)
                .WithOptional(e => e.Genre1)
                .HasForeignKey(e => e.Genre);
        }
    }
}
