namespace WebApplication4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    [Table("Production.ProductReview")]
    public partial class ProductReview
    {
        public ProductReview() { }

        public ProductReview(int ProductReviewID, int ProductID, string ReviewerName, string EmailAddress, int Rating, string Comments)
        {
            this.ProductReviewID = ProductReviewID;
            this.ProductID = ProductID;
            this.ReviewerName = ReviewerName;
            this.EmailAddress = EmailAddress;
            this.Rating = Rating;
            this.Comments = Comments;
            this.ReviewDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            /*
            using (ProductsContext db = new ProductsContext())
            {
                this.Product = db.Products.Where(x => x.ProductID == ProductID).First();
            }
            */
        }

        public int ProductReviewID { get; set; }

        public int ProductID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string ReviewerName { get; set; }

        public DateTime ReviewDate { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        public int Rating { get; set; }

        [StringLength(3850)]
        public string Comments { get; set; }

        public DateTime ModifiedDate { get; set; }

        public virtual Product Product { get; set; }
    }
}