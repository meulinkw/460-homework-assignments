﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;
using System.Net;

namespace WebApplication4.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sub(int? id)
        {
            if (id == null)
            {
                id = 1;
            }
            using (var db = new ProductContext())
            {
                var ProductCategory = db.ProductCategories.Where(x => x.ProductCategoryID == id).First();
                ViewBag.Title = ProductCategory.Name;
                return View(ProductCategory.ProductSubcategories.ToList());
            }
        }

        public ActionResult ViewProducts(int? id)
        {
            if (id == null)
            {
                id = 1;
            }
            using (var db = new ProductContext())
            {
                var SubCategory = db.ProductSubcategories.Where(x => x.ProductSubcategoryID == id).First();
                return View(SubCategory.Products.ToList());
            }
        }
        [HttpGet]
        public ActionResult ProductDetails(int? id)
        {
            int pid = id ?? 0;
            if (pid == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var db = new ProductContext())
            {

                Product curProduct = db.Products.Where(x => x.ProductID == pid).First();
                ProductModelProductDescriptionCulture productDescriptionAndCulture = db.ProductModelProductDescriptionCultures.Where(x => x.ProductModelID == curProduct.ProductModelID).First();
                ProductDescription descriptionObject = db.ProductDescriptions.Where(x => x.ProductDescriptionID == productDescriptionAndCulture.ProductDescriptionID).First();

                ViewBag.productName = curProduct.Name;
                ViewBag.curReviews = db.ProductReviews.Where(x => x.ProductID == curProduct.ProductID).ToList();

                ProductReview newReview = db.ProductReviews.Create();
                newReview.ProductID = curProduct.ProductID;
                newReview.ProductReviewID = db.Products.Count() + 1;
                newReview.ReviewDate = newReview.ModifiedDate = DateTime.Now;
                return View(newReview);
            }
        }

        [HttpPost]
        public ActionResult ProductDetails(ProductReview newReview)
        {
            using (var db = new ProductContext())
            {

                if (ModelState.IsValid)
                {
                    db.ProductReviews.Add(newReview);
                    db.SaveChanges();
                    return RedirectToAction("ProductDetails", new { id = newReview.ProductID });
                }

                return RedirectToAction("Index");
            }


        }


    }
}