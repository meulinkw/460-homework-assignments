﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Homework6.Models;

namespace Homework6.Controllers
{
    public class HomeController : Controller
    {
        private DBContext db = new DBContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSubCategories(string category)
        {
            var catID = db.ProductCategories.FirstOrDefault(x => x.Name == category).ProductCategoryID;
            return View(db.ProductSubcategories.Where(x => x.ProductCategory.ProductCategoryID == catID));
        }

        public ActionResult GetProducts(string subcategory)
        {
            var subcatID = db.ProductSubcategories.FirstOrDefault(x => x.Name == subcategory).ProductSubcategoryID;
            return View(db.Products.Where(x => x.ProductSubcategoryID == subcatID));
        }

        public ActionResult GetProductInfo(int productID)
        {
            var product = db.Products.FirstOrDefault(x => x.ProductID == productID);
            return View(product);
        }

    }
}